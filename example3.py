from itertools import combinations

from sklearn import metrics
import networkx as nx
import numpy as np

from segnet import segnet
reload(segnet)

segnet = segnet.SegNet(dimensions=128, p=1, q=0.1, walk_length=5,
                       negative=5, num_walks=20, n_iter=20)

G = nx.gaussian_random_partition_graph(1000, 10, 20, .25, .07)

random_nodes = np.arange(1000)
np.random.shuffle(random_nodes)
so_far = 0
part2nodes = {}
for part, nodes in enumerate(G.graph['partition']):
    # nodes = random_nodes[so_far:so_far+len(nodes)]
    # so_far += len(nodes)
    part2nodes['Part-%d' % part] = set(nodes)

segnet.learn_embeddings(graph=G, subgraphs=part2nodes) #{}
part0_nodes = part2nodes['Part-0']

# 1
similar = segnet.similar_nodes_for_subgraph(['Part-0'])
similar_nodes = zip(*similar)[0]
print 'Overlap (DBOW+w): %d ' % len(set(similar_nodes).intersection(part0_nodes))

# 2
par = np.mean(segnet.get_node_vecs(part0_nodes), 0)[None, :]
similar = segnet.model.most_similar(par)
similar = [(int(nid), v) for nid, v in similar]
similar_nodes = zip(*similar)[0]
print 'Overlap (baseline): %d ' % len(set(similar_nodes).intersection(part0_nodes))

# 3
final_score = []
for part, part_nodes in part2nodes.items():
    sim_score = []
    for n1, n2 in combinations(part_nodes, 2):
        v1 = segnet.get_node_vecs([n1])
        v2 = segnet.get_node_vecs([n2])
        sim_score.append(metrics.pairwise.cosine_similarity(v1, v2)[0,0])
    part_score = np.mean(sim_score)
    print 'Part: %s M: %5.3f' % (part, part_score)
    final_score.append(part_score)
print 'Final: %5.3f' % np.mean(final_score)


# -----------------------
# Subgraphs are network neighborhoods
# Final: 0.121 (segnet)
# Final: 0.119 (node2vec)

# Subgraphs are not network neighborhoods
# Final: 0.071 (segnet)
# Final: 0.065 (node2vec)
# -----------------------
