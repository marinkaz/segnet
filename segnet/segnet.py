from collections import defaultdict
from itertools import combinations

import numpy as np
import networkx as nx
from gensim.models import Doc2Vec
from gensim.models.doc2vec import LabeledSentence

import walks

class SegNet(object):

    def __init__(self, dimensions=128, walk_length=5, num_walks=20,
                 window_size=10, n_iter=20, workers=8, p=1, q=0.5,
                 dm=0, dm_mean=0, dm_concat=0, dm_tag_count=1, dbow_words=1,
                 hs=0, negative=5, connective_weight=0.1):
        # (!!!! - if dm=0 then `dbow_words' must be 1 otherwise there is no
        #  training of node vectors)
        self.dimensions = dimensions
        self.walk_length = walk_length
        self.num_walks = num_walks
        self.window_size = window_size
        self.n_iter = n_iter
        self.workers = workers
        self.p = p
        self.q = q
        self.dm = dm
        self.dm_mean = dm_mean
        self.dm_concat = dm_concat
        self.dm_tag_count = dm_tag_count
        self.dbow_words = dbow_words
        self.hs = hs
        self.negative = negative
        self.connective_weight = connective_weight

    def learn_embeddings(self, graph, subgraphs, weighted=False, directed=False):
        self.G = self.read_graph(graph, weighted, directed)
        self.node2subgraphs, self.subgraph2nodes, \
            self.all_subgraphs = self.read_subgraphs(subgraphs)

        self.augment_graph(self.G, self.node2subgraphs, self.subgraph2nodes, self.all_subgraphs)
        self.clean_graph(self.G)

        self.all_subgraphs = set([sg for sgs in self.node2subgraphs.values() for sg in sgs])
        print 'N: %d S:%d' % (self.G.number_of_nodes(), len(self.all_subgraphs))
        for node in self.node2subgraphs:
            assert node in self.G.nodes(), 'Problems'

        self.sampler = walks.Walks(self.G, directed, self.p, self.q)
        sampled_walks = self.sampler.simulate_walks(self.num_walks, self.walk_length)

        self.tagged_walks = []
        for walk in sampled_walks:
            # annotate a walk with all sets that have nodes in the walk
            subgraph_tags = set(
                [sg for n in walk for sg in self.node2subgraphs[n]
                 if n in self.node2subgraphs]
            )
            ls = LabeledSentence(words=map(str, walk), tags=subgraph_tags)
            self.tagged_walks.append(ls)

        self.model = Doc2Vec(size=self.dimensions, window=self.window_size,
            workers=self.workers, min_count=0, hs=self.hs, negative=self.negative,
            dm=self.dm, dm_mean=self.dm_mean, dm_concat=self.dm_concat,
            dm_tag_count=self.dm_tag_count, dbow_words=self.dbow_words,
            iter=self.n_iter, alpha=0.055, min_alpha=0.055)
        # Since the learning rate decreases over the course of iterating over the data,
        # labels which are only seen in a single LabeledSentence during training will
        # only be trained with a fixed learning rate. This frequently produces less
        # than optimal results.
        #
        # Better results are obtained by iterating over the data several times and either:
        # 1. randomize the order of input sentences, or
        # 2. manually control the learning rate over the course of several iterations.
        self.model.build_vocab(self.tagged_walks)
        for epoch in range(self.n_iter):
            print 'Epoch: %d' % epoch
            self.model.train(self.tagged_walks)
            self.model.alpha -= 0.002
            self.model.min_alpha = self.model.alpha

    def clean_graph(self, graph):
        # remove edges with zero weight
        to_remove = [(u, v) for (u, v, d) in graph.edges(data=True) if d['weight'] <= 0.0]
        graph.remove_edges_from(to_remove)

    def augment_graph(self, graph, node2subgraphs, subgraph2nodes, all_subgraphs):
        # augment input graph to model disconnected subgraphs
        for subgraph in all_subgraphs:
            nodes = subgraph2nodes[subgraph]
            induced = graph.subgraph(nodes)
            if nx.number_connected_components(induced) > 1:
                cc = list(nx.connected_component_subgraphs(induced))
                for cc1, cc2 in combinations(cc, 2):
                    for node1 in cc1.nodes():
                        for node2 in cc2.nodes():
                            if not graph.has_edge(node1, node2):
                                graph.add_edge(node1, node2)
                                graph[node1][node2]['weight'] = self.connective_weight

    def infer_subgraph_vec(self, nodes, alpha=0.1, min_alpha=0.0001, steps=5):
        # sampled_walks = self.sampler.simulate_walks(self.num_walks, self.walk_length, nodes)
        # sampled_walks = [map(str, walk) for walk in sampled_walks]
        vec = self.model.infer_vector(map(str, nodes), alpha, min_alpha, steps)
        return vec

    def get_node_vecs(self, ids):
        for nid in ids:
            assert nid in self.G.nodes(), 'Problems'
        vecs = np.array([self.model[str(nid)] for nid in ids])
        return vecs

    def get_subgraph_vecs(self, ids):
        for sid in ids:
            assert sid in self.all_subgraphs, 'Problems'
        vecs = np.array([self.model.docvecs[sid] for sid in ids])
        return vecs

    def similar_nodes_for_subgraph(self, positive=[], negative=[], topn=10,
                              restrict_vocab=None, indexer=None):
        positive = self.get_subgraph_vecs(positive)
        negative = self.get_subgraph_vecs(negative)
        similar = self.model.most_similar(
            positive, negative, topn, restrict_vocab, indexer)
        if type(self.G.nodes()[0]) == int and topn is not False:
            # we have integer node ids
            similar = [(int(nid), v) for nid, v in similar]
        return similar

    def similar_subgraphs_for_node(self, positive=[], negative=[], topn=10,
                              restrict_vocab=None, indexer=None):
        positive = self.get_node_vecs(positive)
        negative = self.get_node_vecs(negative)
        similar = self.model.docvecs.most_similar(
            positive, negative, topn, restrict_vocab, indexer)
        return similar

    def similar_nodes_for_node(self, positive=[], negative=[], topn=10,
                          restrict_vocab=None, indexer=None):
        similar = self.model.most_similar(
            map(str, positive), map(str, negative), topn, restrict_vocab, indexer)
        if type(self.G.nodes()[0]) == int and topn is not False:
            # we have integer node ids
            similar = [(int(nid), v) for nid, v in similar]
        return similar

    def similar_subgraphs_for_subgraph(self, positive=[], negative=[], topn=10,
                              restrict_vocab=None, indexer=None):
        similar = self.model.docvecs.most_similar(
            positive, negative, topn, restrict_vocab, indexer)
        return similar

    def save_node_vecs(self, output_path):
        self.model.save_word2vec_format(output_path)

    def save_subgraph_vecs(self, output_path):
        with open(output_path, 'w') as fout:
            fout.write('%d %d\n' % (len(self.all_subgraphs), len(self.model.docvecs[0])))
            for subgraph in self.all_subgraphs:
                vec = self.model.docvecs[subgraph]
                fout.write('%s %s\n' % (subgraph, ' '.join(map(str, vec))))

    def read_graph(self, graph, weighted, directed):
        if isinstance(graph, np.ndarray):
            return nx.from_numpy_matrix(np.array(graph))
        elif isinstance(graph, nx.Graph) or isinstance(graph, nx.DiGraph):
            edge_attr = graph.edges(data=True)[0][2]
            if 'weight' not in edge_attr:
                for edge in graph.edges():
                    graph[edge[0]][edge[1]]['weight'] = 1
            if not directed:
                graph = graph.to_undirected()
            return graph
        else:
            if weighted:
                graph = nx.read_edgelist(
                    graph, nodetype=int, data=(('weight', float),),
                    create_using=nx.DiGraph())
            else:
                graph = nx.read_edgelist(
                    graph, nodetype=int, create_using=nx.DiGraph())
                for edge in graph.edges():
                    graph[edge[0]][edge[1]]['weight'] = 1

            if not directed:
                graph = graph.to_undirected()
            return graph

    def read_subgraphs(self, subgraphs):
        all_subgraphs = set()
        node2subgraphs = defaultdict(set)
        # inverse dictionary
        subgraph2nodes = defaultdict(set)
        if type(subgraphs) == type(dict()) or \
                        type(subgraphs) == type(defaultdict):
            for subgraph, nodes in subgraphs.iteritems():
                all_subgraphs.add(subgraph)
                for node in nodes:
                    node2subgraphs[node].add(subgraph)
                    subgraph2nodes[subgraph].add(node)
        else:
            with open(subgraphs) as fin:
                for line in fin:
                    line = line.strip().split()
                    subgraph, nodes = line[0], map(int, line[1:])
                    all_subgraphs.add(subgraph)
                    for node in nodes:
                        node2subgraphs[node].add(subgraph)
                        subgraph2nodes[subgraph].add(node)
        return node2subgraphs, subgraph2nodes, all_subgraphs
