import networkx as nx
import numpy as np

from segnet import segnet

# model initialization
sg2v = segnet.SegNet()

# learning embeddings
G = nx.read_edgelist('net/karate.edgelist')
adj = nx.adjacency_matrix(G).todense()
subgraph2nodes = {
    'RNDTAG1': {1},
    'RNDTAG2': {1, 26},
    'RNDTAG3': {30, 10, 11, 12, 14, 15},
    'RNDTAG4': {30, 10, 11, 12, 14, 15}}

sg2v.learn_embeddings(graph=adj, subgraphs=subgraph2nodes)

# get learned subgraph and node vectors
v1 = sg2v.get_subgraph_vecs(['RNDTAG1'])
v2 =  sg2v.get_node_vecs([1])
