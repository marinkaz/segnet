# Distributed representations of nodes and subgraphs

### Relevant related work

- Le and Mikolov, Distributed representations of sentences and documents, ICML 2014.
- Grover and Leskovec, Node2vec: scalable feature learning for networks, KDD 2016.


### Contributors:

* Monica Agrawal, <agrawalm@stanford.edu>
* Marinka Zitnik, <marinka@stanford.edu>
