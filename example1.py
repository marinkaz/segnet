from sklearn import metrics

from segnet import segnet

# model initialization
sg2v = segnet.SegNet()

# learning embeddings
net_path = 'net/karate.edgelist'
subgraph_path = 'net/karate.subgraphs'
sg2v.learn_embeddings(graph=net_path, subgraphs=subgraph_path)

# get learned subgraph and node vectors
v1 = sg2v.get_subgraph_vecs(['RNDTAG1'])
v2 =  sg2v.get_node_vecs([1])

# infer vectors for new subgraphs
vec = sg2v.infer_subgraph_vec([30, 10, 11, 12, 14, 15])
tags = ['RNDTAG1', 'RNDTAG2', 'RNDTAG3', 'RNDTAG4']
for tag in tags:
    sim = metrics.pairwise.cosine_similarity(vec, sg2v.get_subgraph_vecs([tag]))
    print tag, sim

# completely overlapping tags
for i in range(len(tags)):
    for j in range(i+1, len(tags)):
        vec1 = sg2v.get_subgraph_vecs([tags[i]])
        vec2 = sg2v.get_subgraph_vecs([tags[j]])
        sim = metrics.pairwise.cosine_similarity(vec1, vec2)
        print tags[i], tags[j], sim

# save subgraph and node vectors
sg2v.save_node_vecs('karate_nodes.emb')
sg2v.save_subgraph_vecs('karate_subgraphs.emb')
